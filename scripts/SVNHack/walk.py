#!/usr/bin/env python

import re
import os
import sys

# since the simple webshell would not be too long
max_file_size = 10000

# shell re
shell_re = [ '(eval\(\$_)(POST|GET|REQUEST)\[(.*?)\]\)'
]

dir_name  = sys.argv[1]
if not os.path.exists(dir_name):
    sys.exit('no such dir!')


for root, dirs, files in os.walk(dir_name):
    for name in files:
	file_path = os.path.join(root,name)
	file_size = os.path.getsize(file_path)
	if file_size > max_file_size:
	    continue
	for re_rule in shell_re:
	    res = re.search(re_rule,open(file_path,'r').read())
	    if res:
		pwd = re.findall(re_rule,open(file_path,'r').read())[0][2]
		print file_path + '||||' + res.group() + '||||' + pwd


