#!/usr/bin/env python
# -*- coding -*- :utf8

import requests
import re
import socket,socks
import nmap
import sys
import config
import log
import os
from urllib import quote
import time
import eventlet
from eventlet.timeout import Timeout
from FastCGIClient import *
import json

# patch for the timeout
# eventlet.monkey_patch()

class ServerScan():

	def __init__(self,proxy_port,ip,server_port):
		self.proxy_port = proxy_port
		self.ip = ip
		self.server_port = server_port
		self.timeout = config.timeout
		self.server_exp = config.server_exp

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket


	def scan(self):
		'''
			1. If the port for each service is fixed, then traverse the port to determine which kind of scanner should be used;
			2. If the port for each service is flexible, then traverse the port with whole the payload going to be used;

		'''

		scan_array = [self.phpMyAdmin,self.shellShock,self.tomcat,self.jboss,self.mysql,self.ssh,self.fpmfastcgi,self.exim,self.samba]
		self.server_id = {}


		for port in self.server_port:
			scan_func_indexes = config.server_port_map[str(port)]
			for scan_func_index in scan_func_indexes:
				scan_func = scan_array[scan_func_index]

				with Timeout(config.timeout,False):
					tmp_res = scan_func(port)
				
				if tmp_res != -1:
					self.server_id[str(port)] = tmp_res
					break

			if not self.server_id.has_key(str(port)):
				self.server_id[str(port)] = 'server_unknown'

		return json.dumps(self.server_id)


	def phpMyAdmin(self,port):

		url1 = "http://"+ self.ip + ":" + str(port)+"/phpmyadmin/"
		content1 = ""
		try:
			# not a web
			if not requests.head(url1,timeout=self.timeout * 1000).status_code == 200:
				return -1
			content1 = requests.get(url1,timeout=self.timeout * 1000).content
			if content1.find("phpMyAdmin")!=-1:
				try:
					pyaVersion = re.compile(r'(?<=PMA_VERSION:")([\s\S]*?)(?=",auth_type)').findall(content1.decode("utf-8"))
					if pyaVersion[0]=="4.8.1":
						return self.server_exp['phpMyAdmin_1']
					else:
						return self.server_exp['phpMyAdmin_2']
				except:
					pass
		except Exception,e:
			if config.debug:
				print e
		return -1
		

	def shellShock(self,port):

		url1 = "http://"+ self.ip + ":"+ str(port)+"/victim.cgi"
		try:
			if requests.head(url1,timeout=self.timeout * 1000).status_code == 200:
				return self.server_exp['shellShock']
		except Exception,e:
			if config.debug:
				print e
		return -1


	def tomcat(self,port):

		url = "http://"+ self.ip + ":" + str(port) + "/"
		try:
			if requests.get(url1+"manager/html",timeout=self.timeout * 1000).status_code == 401:
				return self.server_exp['tomcat']
			elif requests.get(url1+"manager/text",timeout=self.timeout * 1000).status_code == 401:
				return self.server_exp['tomcat']
			else:
				content = requests.get(url1,timeout=self.timeout * 1000).content
				if content.find("Apache Software Foundation")!=-1:
					return self.server_exp['tomcat']
		except Exception,e:
			if config.debug:
				print e
			return -1


	def mysql(self,port):

		try:
			client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			client.settimeout(self.timeout)
			client.connect((self.ip,port))
			client.send("test")
			response = client.recv(4096)
			if response.find("mysql")!=-1:
				return self.server_exp['mysql']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
			return -1


	def ssh(self,port):

		try:
			client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			client.settimeout(self.timeout)
			client.connect((self.ip,port))
			client.send("test")
			response = client.recv(128)
			if response.find("SSH")!=-1:
				return self.server_exp['ssh']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
			return -1


	def jboss(self,port):

		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			if requests.get(url1+"jbossmq-httpil/HTTPServerILServlet").status_code == 200:
				return self.server_exp['jboss']
			elif requests.get(url1).content.find("JBoss Online Resources")!=-1:
				return self.server_exp['jboss']
			elif requests.get(url1).content.find("JBoss Management") !=-1:
				return self.server_exp['jboss']
			elif requests.get(url1+"web-console/").status_code == 401:
				return self.server_exp['jboss']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
			return -1


	def samba(self,port):

		try:
			nm = nmap.PortScanner()
			nm.scan(self.ip,str(port))
			sambainfo = nm[target_host]['tcp'][target_port]['product']
			if sambainfo.find("Samba")!=-1:
				return self.server_exp['samba']
		except Exception,e:
			if config.debug:
				print e
			return -1


	def fpmfastcgi(self,port):
		return -1
		client = FastCGIClient(self.ip, port , self.timeout, 0)
		params = dict()
		documentRoot = "/var/www/html/"
		uri = "index.php"
		content = "hello fastcgi"
		params = {'GATEWAY_INTERFACE': 'FastCGI/1.0',
				  'REQUEST_METHOD': 'POST',
				  'SCRIPT_FILENAME': documentRoot + uri,
				   'SCRIPT_NAME': uri,
				   'QUERY_STRING': '',
				   'REQUEST_URI': uri,
				   'DOCUMENT_ROOT': documentRoot,
				   'SERVER_SOFTWARE': 'php/fcgiclient',
				   'REMOTE_ADDR': '127.0.0.1',
				   'REMOTE_PORT': '9985',
				   'SERVER_ADDR': '127.0.0.1',
				   'SERVER_PORT': '80',
					'SERVER_NAME': "localhost",
				   'SERVER_PROTOCOL': 'HTTP/1.1',
				   'CONTENT_TYPE': 'application/x-www-form-urlencoded',
				   'CONTENT_LENGTH': len(content)
				  }
		try:
			response = client.request(params, content)
			if response.find("fastcgi"):
				return self.server_exp['php-fpm']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
			return -1


	def exim(self,port):

		try:
			client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			client.settimeout(self.timeout)
			client.connect((self.ip,port))
			client.send("test")
			response = client.recv(4096)
			if response.find("SMTP")!=-1:
				return self.server_exp['exim']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
			return -1

if __name__ == '__main__':
	# set the debug to 0 to aviod the interference from the output
	config.debug = 0
	s = ServerScan(int(sys.argv[1]),sys.argv[2],map(int,sys.argv[3].split(',')))
	print s.scan()
