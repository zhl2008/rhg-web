#!/usr/bin/env python

'''
	configuration for the game

'''

# debug mode
debug = False

# username for the challenge interface
#user = 'user13'
user = 'team06'
user = 'admin'

# password for the challenge interface
#pwd = '524103'
pwd = '5ACPJ54p'
pwd = '123456'

# url for the interface
interface = 'https://rhg.ichunqiu.com/rhg'
interface = 'http://39.97.183.153:5000/rhg'
#interface = 'https://172.16.4.1'

# interface for getting the status
status_url = '/api/get_question_status'

# interface for submitting the flag
flag_url = '/api/sub_answer'

# interface for resetting
reset_url = '/api/reset_question'

# interface for chekcing 
check_url = '/api/call_question_check'

# flag path
flag_path = '/tmp/flag'

# interface for getting check info
check_info_url = '/api/get_check_info'

# interface for alive detect
heartbeat_url = '/api/heartbeat' 

# runtime path
runtime_path = '/tmp'

# tool path (portscan and proxy)
tool_path = './tool'

# log file
log_file = 'my_log'

# whether write logs to file
log_to_file = 1

# attack/defense thread num
exploit_thread = 5

# thread watch time span
thread_watch_span = 10

# proxy start port
proxy_start_port = 18800

# port_list to be scanned
port_list = [22,25,80,445,3306,8080,9000]

# timeout for http connection and sockets
timeout = 4

# ip of myself
self_ip = '39.97.182.15'

# port of myself
self_port = 8888


# config download path of frp
frp_down_path = 'http://%s:%s/frp' %(self_ip, str(self_port))

down_path = 'http://%s:%s' %(self_ip, str(self_port))

# the web exp list
web_exp = {
	"5.0.3-web1":"web_1",
	"5.0.3-web2":"web_2",
	"5.0.3-web3":"web_3",
	"5.0.3-web10":"web_10",
	"4.5.3-web4":"web_4",
	"5.1-web5":"web_5",
	"5.1-web6":"web_6",
	"5.1-web7":"web_7",
	"4.5.1-web8":"web_8",
	"4.5-web9":"web_9",
	"HelloWorld":"web_test"
}

# the server exp list
server_exp = {
	"phpMyAdmin_1":"server_1",
	"mysql":"server_2",
	"tomcat" : "server_3",
	"ssh" : "server_4",
	"phpMyAdmin_2" : "server_5",
	"samba" : "server_6",
	"jboss " : "server_7",
	"php-fpm" : "server_8",
	"exim" : "server_9",
	"shellShock" : "server_10",
	"HelloWorld" : "server_test"
}

# server port mapping
# scan_array = [self.phpMyAdmin,self.shellShock,self.tomcat,self.jboss,self.mysql,self.ssh,self.fpmfastcgi,self.exim,self.samba]

server_port_map = {
	"22": [5],
	"25": [7],
	"80": [0,1],
	"445": [8],
	"3306": [4],
	"8080": [2,3],
	"9000": [6]
}

server_port_exp_map = {
	"22": ['server_4','server_test'],
	"25": ['server_9','server_test'],
	"80": ['server_1','server_5','server_10','server_test'],
	"445": ['server_6','server_test'],
	"3306": ['server_2','server_test'],
	"8080": ['server_3','server_7','server_test'],
	"9000": ['server_8','server_test']
}



